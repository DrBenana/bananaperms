package me.benana.bananaapiperms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.events.AddPermissionToPlayerEvent;
import me.benana.bananaapiperms.events.DelPermissionFromPlayerEvent;
import me.benana.bananaapiperms.events.PlayerMovedGroupEvent;
import me.benana.bananaperms.FileManager;
import me.benana.bananaperms.Main;
import me.benana.bananaperms.SimpleConfig;


public class uBananaPerms {
	
	private JavaPlugin plugin;
	private SimpleConfig config;
	private String uuid;
	private HashMap<String, PermissionAttachment> aPermissions = new HashMap<>();
	public static HashMap<String, ArrayList<String>> Permissions = new HashMap<>();
	
	public uBananaPerms(JavaPlugin pl, UUID UniqueID) {
		this.plugin = pl;
		this.uuid = UniqueID.toString();
		FileManager fm = new FileManager(pl);
		try {
			this.config = fm.getNewConfig("users.yml");
		} catch (Exception e) {
			
		}
	}
	
	public uBananaPerms(JavaPlugin pl, String UniqueID) {
		this.plugin = pl;
		this.uuid = UniqueID;
		FileManager fm = new FileManager(pl);
		try {
		this.config = fm.getNewConfig("users.yml");
		} catch (Exception e) {
			
		}
	}
	
	public uBananaPerms(UUID UniqueID) {
		this(Main.helper.getJavaPlugin(), UniqueID);
	}
	
	public uBananaPerms(String UniqueID) {
		this(Main.helper.getJavaPlugin(), UniqueID);
	}
	
	public void addPermission(String permission) {
		setPermission(permission, true);
		Bukkit.getServer().getPluginManager().callEvent(new AddPermissionToPlayerEvent(this.plugin, this.uuid, permission));
		ArrayList<String> prms = this.getConfigPermissions();
		prms.add(permission);
		this.config.set("User." + this.uuid + ".Permissions", prms);
		this.config.saveConfig();
	}
	
	public void delPermission(String permission) {
		setPermission(permission, false);
		Bukkit.getServer().getPluginManager().callEvent(new DelPermissionFromPlayerEvent(this.plugin, this.uuid, permission));
		ArrayList<String> prms = this.getConfigPermissions();
		prms.remove(permission);
		this.config.set("User." + this.uuid + ".Permissions", prms);
		this.config.saveConfig();
	}
	
	public void setFormat(String format) {
		config.set("User." + this.uuid + ".Format", format);
		config.saveConfig();
	}
	
	public String getFormat() {
		return config.getString("User." + this.uuid + ".Format");
	}
	
	@SuppressWarnings({"unchecked" })
	public ArrayList<String> getConfigPermissions() {
		if (this.config.getList("User." + this.uuid + ".Permissions") == null) return new ArrayList<>();
		ArrayList<String> prms = (ArrayList<String>) this.config.getList("User." + this.uuid + ".Permissions");
		return prms;
	}
	
	public ArrayList<String> getPermissions() {
		if (!Permissions.containsKey(this.uuid)) return new ArrayList<>();
		return Permissions.get(this.uuid);
	}
	
	public String getPlayerGroup() {
		if (this.config.getString("User." + this.uuid + ".Group") == null) return gBananaPerms.getDefaultGroup(this.plugin);
		return this.config.getString("User." + this.uuid + ".Group");
	}
	
	public void setPlayerGroup(String group) {
		Bukkit.getServer().getPluginManager().callEvent(new PlayerMovedGroupEvent(this.plugin, this.uuid, this.getPlayerGroup(), group));
		this.config.set("User." + this.uuid + ".Group", group);
		this.config.saveConfig();
	}
	
	public void setUUID(UUID u) {
		this.uuid = u.toString();
	}
	
	public void setUUID(String u) {
		this.uuid = u;
	}
	
	public String getUUID() {
		return this.uuid;
	}
	
	private PermissionAttachment getPT(Player p) {
		if (!aPermissions.containsKey(this.uuid)) {
			aPermissions.put(this.uuid, p.addAttachment(this.plugin));
		}
		return aPermissions.get(this.uuid);
	}
	
	public void setPermission(String permission, boolean b) {
		if (b) {
			try {
				ArrayList<String> t = Permissions.get(this.uuid);
				t.add(permission);
				Permissions.replace(this.uuid, t);
			} catch (Exception e) {
				ArrayList<String> t = new ArrayList<>();
				t.add(permission);
				Permissions.put(this.uuid, t);
			}
		} else {
			try {
				ArrayList<String> t = Permissions.get(this.uuid);
				t.remove(permission);
				Permissions.replace(this.uuid, t);
			} catch (Exception e) {
				Logger.getLogger("Minecraft").warning("[BananaPerms] Error with set the permissions.");
			}
		}
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.getUniqueId().equals(UUID.fromString(this.uuid))) {
				PermissionAttachment at = getPT(p);
				at.setPermission(permission, b);
				aPermissions.replace(this.uuid, at);
			}
		}
	}
	
}
