package me.benana.bananaapiperms.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.uBananaPerms;

public class AddPermissionToPlayerEvent extends Event {
	private JavaPlugin plugin;
	private String uuid, permission;
	
	public AddPermissionToPlayerEvent(JavaPlugin pl, String player, String perm) {
		this.plugin = pl;
		this.uuid = player;
		this.permission = perm;
	}
	
	public String getPlayerUUID() {
		return uuid;
	}
	
	public String getPermission() {
		return this.permission;
	}
	
	public uBananaPerms getBananaPerms() {
		return new uBananaPerms(this.plugin, this.uuid);
	}
	
	public void setCancelled() {
		this.getBananaPerms().delPermission(this.permission);
	}
	
	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
	    return handlers;
	}

	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
