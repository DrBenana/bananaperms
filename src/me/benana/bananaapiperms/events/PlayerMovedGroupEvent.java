package me.benana.bananaapiperms.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.gBananaPerms;
import me.benana.bananaapiperms.uBananaPerms;

public class PlayerMovedGroupEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private JavaPlugin plugin;
	private String first, second, p;
	
	public PlayerMovedGroupEvent(JavaPlugin main, String uuidplayer, String firstgroup, String secondgroup) {
		this.plugin = main;
		this.first = firstgroup;
		this.second = secondgroup;
		this.p = uuidplayer;
	}
	
	public uBananaPerms getUserBananaPerms() {
		return new uBananaPerms(this.plugin, this.p);
	}
	
	public gBananaPerms getFirstGroupBananaPerms() {
		return new gBananaPerms(this.plugin, this.first);
	}
	
	public gBananaPerms getSecondGroupBananaPerms() {
		return new gBananaPerms(this.plugin, this.second);
	}
	
	public String getFirstGroupName() {
		return this.first;
	}
	
	public String getSecondGroupName() {
		return this.second;
	}
	
	public String getPlayerUUID() {
		return this.p;
	}
	
	public void setCancelled() {
		this.getUserBananaPerms().setPlayerGroup(this.first);
	}
	
	public JavaPlugin getJavaPlugin() {
		return this.plugin;
	}

	
	public HandlerList getHandlers() {
	    return handlers;
	}

	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
