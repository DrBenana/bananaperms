package me.benana.bananaapiperms.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.gBananaPerms;

public class AddPermissionToGroupEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private JavaPlugin plugin;
	private String group, permission;
	
	public AddPermissionToGroupEvent(JavaPlugin main, String groupname, String perm) {
		this.plugin = main;
		this.group = groupname;
		this.permission = perm;
	}
	
	public gBananaPerms getBananaPerms() {
		return new gBananaPerms(this.plugin, this.group);
	}
	
	public String getGroupName() {
		return this.group;
	}
	
	public String getPermission() {
		return this.permission;
	}
	
	public void setCancelled() {
		this.getBananaPerms().delPermission(this.permission);
	}
	
	public JavaPlugin getJavaPlugin() {
		return this.plugin;
	}
	
	public HandlerList getHandlers() {
	    return handlers;
	}

	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
