package me.benana.bananaapiperms;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.events.AddPermissionToGroupEvent;
import me.benana.bananaapiperms.events.DelPermissionFromGroupEvent;
import me.benana.bananaperms.FileManager;
import me.benana.bananaperms.Main;
import me.benana.bananaperms.SimpleConfig;


public class gBananaPerms {
	
	private JavaPlugin plugin;
	private SimpleConfig config;
	private String uuid;
		
	public gBananaPerms(JavaPlugin pl, String UniqueID) {
		this.plugin = pl;
		this.uuid = UniqueID;
		FileManager fm = new FileManager(pl);
		try {
			this.config = fm.getNewConfig("groups.yml");
		} catch (Exception e) {
			
		}
	}
	
	public gBananaPerms(String UniqueID) {
		this(Main.helper.getJavaPlugin(), UniqueID);
	}
	
	public void addPermission(String permission) {
		ArrayList<String> prms = this.getPermissions();
		prms.add(permission);
		this.config.set("User." + this.uuid + ".Permissions", prms);
		this.config.saveConfig();
		Bukkit.getServer().getPluginManager().callEvent(new AddPermissionToGroupEvent(this.plugin, this.uuid, permission));
	}
	
	public void delPermission(String permission) {
		ArrayList<String> prms = this.getPermissions();
		prms.remove(permission);
		this.config.set("User." + this.uuid + ".Permissions", prms);
		this.config.saveConfig();
		Bukkit.getServer().getPluginManager().callEvent(new DelPermissionFromGroupEvent(this.plugin, this.uuid, permission));
	}
	
	public void setFormat(String format) {
		config.set("User." + this.uuid + ".Format", format);
		config.saveConfig();
	}
	
	public String getFormat() {
		return config.getString("User." + this.uuid + ".Format");
	}
	
	@SuppressWarnings({"unchecked" })
	public ArrayList<String> getPermissions() {
		if (this.config.getList("User." + this.uuid + ".Permissions") == null) return new ArrayList<>();
		ArrayList<String> prms = (ArrayList<String>) this.config.getList("User." + this.uuid + ".Permissions");
		return prms;
	}
	
	public String getDefaultGroup() {
		if (this.plugin.getConfig().getString("Permissions.Groups.Default") == null) return "default";
		return this.plugin.getConfig().getString("Permissions.Groups.Default");
	}
	
	public void setDefaultGroup(String group) {
		this.plugin.getConfig().set("Permissions.Groups.Default", group);
		this.plugin.saveConfig();
	}
	
	public static String getDefaultGroup(JavaPlugin pl) {
		if (pl.getConfig().getString("Permissions.Groups.Default") == null) return "default";
		return pl.getConfig().getString("Permissions.Groups.Default");
	}
	
	public static void setDefaultGroup(JavaPlugin pl, String group) {
		pl.getConfig().set("Permissions.Groups.Default", group);
		pl.saveConfig();
	}
	
	public void setGroup(String u) {
		this.uuid = u;
	}
	
	public String getGroup() {
		return this.uuid;
	}
	
}
