package me.benana.bananaperms.listeners;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.gBananaPerms;
import me.benana.bananaapiperms.uBananaPerms;
import me.benana.bananaapiperms.events.AddPermissionToGroupEvent;
import me.benana.bananaapiperms.events.DelPermissionFromGroupEvent;
import me.benana.bananaapiperms.events.PlayerMovedGroupEvent;

public class PlayerPermissionListener implements Listener {
	private JavaPlugin plugin;
	
	public PlayerPermissionListener(JavaPlugin pl) {
		this.plugin = pl;
	}
	
	
	@EventHandler
	public void playerMoveGroupEvent(PlayerMovedGroupEvent e) {
		uBananaPerms user = e.getUserBananaPerms();
		gBananaPerms first = e.getFirstGroupBananaPerms(), second = e.getSecondGroupBananaPerms();
		
		for (String s : first.getPermissions()) {
			if (user.getPermissions().contains(s)) continue;
			user.setPermission(s, false);
		}
		
		for (String s : second.getPermissions()) {
			user.setPermission(s, true);
		}
	}
	
	@EventHandler
	public void addGrouppToPlayer(AddPermissionToGroupEvent e) {
		uBananaPerms bp = new uBananaPerms(this.plugin, "NOTHING");
		for (Player p : Bukkit.getOnlinePlayers()) {
			bp.setUUID(p.getUniqueId());
			if (bp.getPlayerGroup().equalsIgnoreCase(e.getGroupName())) {
				bp.setPermission(e.getPermission(), true);
			}
		}
	}
	
	@EventHandler
	public void delGrouppFromPlayer(DelPermissionFromGroupEvent e) {
		uBananaPerms bp = new uBananaPerms(this.plugin, "NOTHING");
		for (Player p : Bukkit.getOnlinePlayers()) {
			bp.setUUID(p.getUniqueId());
			if (bp.getPermissions().contains(e.getPermission())) continue;
			if (bp.getPlayerGroup().equalsIgnoreCase(e.getGroupName())) {
				bp.setPermission(e.getPermission(), false);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void addPermissionEvent(PlayerJoinEvent e) {
		uBananaPerms bp = new uBananaPerms(this.plugin, e.getPlayer().getUniqueId());
		gBananaPerms bpg = new gBananaPerms(this.plugin, bp.getPlayerGroup());
		ArrayList<String> permissions = bp.getConfigPermissions();
		for (String s : permissions) {
			bp.setPermission(s, true);
		}
		permissions = bpg.getPermissions();
		for (String s : permissions) {
			bp.setPermission(s, true);
		}
	}
	
	@EventHandler
	public void delPermissionEvent(PlayerQuitEvent e) {
		uBananaPerms bp = new uBananaPerms(this.plugin, e.getPlayer().getUniqueId());
		gBananaPerms bpg = new gBananaPerms(this.plugin, bp.getPlayerGroup());
		@SuppressWarnings("unchecked")
		ArrayList<String> permissions = (ArrayList<String>) bp.getPermissions().clone();
		for (String s : permissions) {
			bp.setPermission(s, false);
		}
		permissions = bpg.getPermissions();
		for (String s : permissions) {
			bp.setPermission(s, false);
		}
	}

}
