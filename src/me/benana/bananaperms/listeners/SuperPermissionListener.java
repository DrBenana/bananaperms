package me.benana.bananaperms.listeners;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;


public class SuperPermissionListener implements Listener {
	private static ArrayList<UUID> op = new ArrayList<>();
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void preOnCommand(PlayerCommandPreprocessEvent e) {
		if (e.getPlayer().hasPermission("*") && !(e.getPlayer().isOp())) {
			e.getPlayer().setOp(true);
			op.add(e.getPlayer().getUniqueId());
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (op.contains(e.getPlayer())) {
			e.getPlayer().setOp(false);
			op.remove(e.getPlayer());
		}
	}
	
}
