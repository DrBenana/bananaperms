package me.benana.bananaperms.listeners;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.gBananaPerms;
import me.benana.bananaapiperms.uBananaPerms;
import net.md_5.bungee.api.ChatColor;

public class ChatPlayerListener implements Listener {
	private JavaPlugin plugin;
	
	public ChatPlayerListener(JavaPlugin main) {
		this.plugin = main;
	}
	
	@EventHandler
	public void chatFormatEvent(AsyncPlayerChatEvent e) {
		// Color Symbol
		String symbol = this.plugin.getConfig().getString("Chat.Format.Symbol");
		// Empty Message Protection
		if (this.plugin.getConfig().getBoolean("Chat.Protection.EmptyMessage")) {
			if (ChatColor.stripColor(e.getMessage().replaceAll(symbol, "§")).isEmpty()) {
				e.setCancelled(true);
				e.getPlayer().sendMessage(ChatColor.RED + "You can't send this message because 'EmptyMessage Protection', Contact admin.");
				return;
			}
		}
		uBananaPerms bp = new uBananaPerms(this.plugin, e.getPlayer().getUniqueId());
		String format = bp.getFormat();
		if (e.getPlayer().hasPermission("bananaperms.chat.colors")) e.setMessage(e.getMessage().replaceAll(symbol, "§"));
		
		/**
		 * Private Chats
		 */
		
		@SuppressWarnings("unchecked")
		ArrayList<String> symbols = (ArrayList<String>) plugin.getConfig().getList("Chat.Private");
		
		for (String s : symbols) {
			if (e.getMessage().startsWith(s)) {
				if (!(e.getPlayer().hasPermission("bananaperms.pchat." + s) || e.getPlayer().hasPermission("bananaperms.*"))) break;
				e.setMessage(e.getMessage().replaceFirst(s, ""));
				e.setMessage(e.getMessage().replaceFirst(" ", ""));
				e.setFormat("[" + s + "] " + e.getPlayer().getName() + ": " + e.getMessage());
				e.getRecipients().clear();
				for (Player p : Bukkit.getOnlinePlayers()) {
					if (!(p.hasPermission("bananaperms.pchat." + s) || e.getPlayer().hasPermission("bananaperms.*"))) continue;
					e.getRecipients().add(p);
				}
				return;
			}
		}
		
		/**
		 * Chat Format
		 */
		
		if (format == null) {
			gBananaPerms bpg = new gBananaPerms(this.plugin, bp.getPlayerGroup());
			String gformat = bpg.getFormat();
			// Return when no have format
			if (gformat == null) {
				return;
			}
			String[] fargs = gformat.split("#");
			// Set the format
			e.setFormat(fargs[0] + e.getPlayer().getName() + fargs[1] + " " + e.getMessage());
			// Global Chat
			if (this.plugin.getConfig().getString("Chat.Format.PerWorld").equalsIgnoreCase("false")) return;
			e.getRecipients().clear();
			// Per world chat
			for (Player player : e.getPlayer().getWorld().getPlayers()) {
				e.getRecipients().add(player);
			}
			return;
		}
		// Chat Format Player
		String[] fargs = format.split("#");
		e.setFormat(fargs[0] + e.getPlayer().getName() + fargs[1] + " " + e.getMessage());
		if (this.plugin.getConfig().getString("Chat.Format.PerWorld").equalsIgnoreCase("false")) return;
		e.getRecipients().clear();
		for (Player player : e.getPlayer().getWorld().getPlayers()) {
			e.getRecipients().add(player);
		}

	
	}
		
}
