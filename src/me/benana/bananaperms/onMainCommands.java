package me.benana.bananaperms;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.gBananaPerms;
import me.benana.bananaapiperms.uBananaPerms;
import net.md_5.bungee.api.ChatColor;

public class onMainCommands implements CommandExecutor {
	JavaPlugin plugin;
	
	public onMainCommands(JavaPlugin main) {
		this.plugin = main;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (cmd.equalsIgnoreCase("bp")) {
			if (args.length == 0) {
				sender.sendMessage(ChatColor.RED + "The command wasn't found, try: /bp help");
			}
			else if (args.length == 1) {
				if (args[0].equalsIgnoreCase("help")) {
					if (!sender.hasPermission("bananaperms.main.help")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					sender.sendMessage(ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + ChatColor.UNDERLINE.toString() + "User Commands: ");
					sender.sendMessage("/bp reload " + ChatColor.ITALIC + "Reload 'BananaPerms' plugin.");
					sender.sendMessage("/bpu help " + ChatColor.ITALIC + "Help about the user interface.");
					sender.sendMessage("/bpg help " + ChatColor.ITALIC + "Help about the group interface.");
				}
				else if (args[0].equalsIgnoreCase("reload")) {
					if (!sender.hasPermission("bananaperms.main.reload")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					sender.sendMessage(ChatColor.GREEN + "The reload was started!");
					uBananaPerms bp = new uBananaPerms(this.plugin, "NOTHING");
					gBananaPerms bpg = new gBananaPerms(this.plugin, "NOTHING");
					ArrayList<String> permissions;
					this.plugin.reloadConfig();
					// Variables					
					for (Player p : Bukkit.getOnlinePlayers()) {
						bp.setUUID(p.getUniqueId());
						bpg.setGroup(bp.getPlayerGroup());
						//OnQuit
						permissions = (ArrayList<String>) bp.getPermissions().clone();
						for (String s : permissions) {
							bp.setPermission(s, false);
						}
						
						//OnJoin
						permissions = bp.getConfigPermissions();
						for (String s : permissions) {
							bp.setPermission(s, true);
						}
						permissions = bpg.getPermissions();
						for (String s : permissions) {
							bp.setPermission(s, true);
						}
					}
					sender.sendMessage(ChatColor.AQUA + "The reload was finished!");
				}
				else {
					sender.sendMessage(ChatColor.RED + "The command wasn't found, try: /bp help");
				}
			}
			else {
				sender.sendMessage(ChatColor.RED + "The command wasn't found, try: /bp help");
			}
		}
		return false;
	}

}
