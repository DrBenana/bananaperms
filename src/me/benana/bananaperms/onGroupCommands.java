package me.benana.bananaperms;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.gBananaPerms;
import net.md_5.bungee.api.ChatColor;

public class onGroupCommands implements CommandExecutor {
	private JavaPlugin plugin;
	public onGroupCommands(Main main) {
		this.plugin = main;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		if (cmd.equalsIgnoreCase("bpg") || cmd.equalsIgnoreCase("bg")) {
			
			if (args.length == 0) {
				sender.sendMessage(ChatColor.RED + "The command wasn't found. Try: /bpg help");
				return true;
			}
			
			if (args.length >= 1) {
				if (args[0].equalsIgnoreCase("help")) {
					if (!sender.hasPermission("bananaperms.group.help")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					
					sender.sendMessage(ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + ChatColor.UNDERLINE.toString() + "Group Commands: ");
					sender.sendMessage("/bpg <Group> " + ChatColor.ITALIC + "List permissions of the group.");
					sender.sendMessage("/bpg <Group> c " + ChatColor.ITALIC + "Clear permissions of the group.");
					sender.sendMessage("/bpg <Group> d " + ChatColor.ITALIC + "Set the group to default.");
					sender.sendMessage("/bpg <Group> + <Permission> " + ChatColor.ITALIC + "Add permission to the group.");
					sender.sendMessage("/bpg <Group> - <Permission> " + ChatColor.ITALIC + "Remove permission from the group.");
					sender.sendMessage("/bpg <Group> f {Suffix#Prefix} " + ChatColor.ITALIC + "Set chat format, #=Username.");
					sender.sendMessage(ChatColor.GOLD + "Default Group: " + gBananaPerms.getDefaultGroup(this.plugin));
					return true;
				}
			}
			gBananaPerms bp = new gBananaPerms(plugin, args[0]);
			
			if (args.length == 1) {
				if (!sender.hasPermission("bananaperms.group.list")) {
					sender.sendMessage(ChatColor.RED + "You don't have permissions.");
					return true;
				}
				
				sender.sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() + "The permissions of " + args[0] + " : ");
				sender.sendMessage("");
				for (String s : bp.getPermissions()) {
					sender.sendMessage("  - " + s);
				}
			}
			
			// Args == 1
			// Args == 2
			
			else if (args.length == 2) {				
				if (args[1].equalsIgnoreCase("c")) {
					if (!sender.hasPermission("bananaperms.group.clear")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					@SuppressWarnings("unchecked")
					ArrayList<String> prms = (ArrayList<String>) bp.getPermissions().clone();
					for (String s : prms) {
						bp.delPermission(s);
					}
					sender.sendMessage(ChatColor.RED + "All the permissions removed.");
				}
				else if (args[1].equalsIgnoreCase("f")) {
					if (!sender.hasPermission("bananaperms.group.chat")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					bp.setFormat(null);
					sender.sendMessage(ChatColor.RED + "The format was removed.");
				}
				else if (args[1].equalsIgnoreCase("d")) {
					if (!sender.hasPermission("bananaperms.group.default")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					if (gBananaPerms.getDefaultGroup(this.plugin).equalsIgnoreCase(args[0])) {
						sender.sendMessage(ChatColor.RED + "The group is already the default.");
						return true;
					}
					gBananaPerms.setDefaultGroup(this.plugin, args[0]);
					sender.sendMessage(ChatColor.DARK_AQUA + "The group was set to default.");
				}
				else {
					sender.sendMessage(ChatColor.RED + "The command wasn't found. Try: /bpg help");
				}
			}
			
			// Args == 2
			// Args == 3
			
			else if (args.length >= 3) {
				if (args[1].equalsIgnoreCase("f")) {
					if (!sender.hasPermission("bananaperms.group.chat")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					
					String message = "";
					for (String s : args) {
						if (s.equals(args[0]) || s.equals(args[1])) {continue;}
						message = message + (s.equals(args[args.length-1]) ? s : s + " ");
					}
					if (!message.contains("#")) {
						sender.sendMessage(ChatColor.RED + "The format isn't contains #");
						return true;
					}
					message = message.replaceAll("&", "§");
					bp.setFormat(message);
					sender.sendMessage(ChatColor.AQUA + "The format is now : " + ChatColor.LIGHT_PURPLE  + message);
					return true;
				}
				
				if (args.length != 3) {
					sender.sendMessage(ChatColor.RED + "The command wasn't found. try: /bpg help");
					return true;
				}
				
				if (args[1].equalsIgnoreCase("+")) {
					if (!sender.hasPermission("bananaperms.group.add")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					
					if (bp.getPermissions().contains(args[2])) {
						sender.sendMessage(ChatColor.RED + "The group has already have the permission.");
						return true;
					}
					
					bp.addPermission(args[2]);
					sender.sendMessage(ChatColor.AQUA + "The permission has been added!");
				}
				else if (args[1].equalsIgnoreCase("-")) {
					if (!sender.hasPermission("bananaperms.group.del")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					
					if (!(bp.getPermissions().contains(args[2]))) {
						sender.sendMessage(ChatColor.RED + "He doesn't have the permission.");
						return true;
					}
					
					bp.delPermission(args[2]);
					sender.sendMessage(ChatColor.AQUA + "The permission has been removed!");
				}
				else {
					sender.sendMessage(ChatColor.RED + "The command wasn't found. Try: /bpu help");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "The command wasn't found. Try: /bpg help");
			}
		}
		return false;
	}

}
