package me.benana.bananaperms;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.uBananaPerms;
import net.md_5.bungee.api.ChatColor;

public class onUserCommands implements CommandExecutor{
	private JavaPlugin plugin;
	
	public onUserCommands(JavaPlugin pl) {
		this.plugin = pl;
	}
	
	@SuppressWarnings({ "deprecation"})
	@Override
	public boolean onCommand(CommandSender sender, Command command, String cmd, String[] args) {
		
		if (cmd.equalsIgnoreCase("bpu") || cmd.equalsIgnoreCase("bu")) {
			
			if (args.length == 0) {
				sender.sendMessage(ChatColor.RED + "The command wasn't found. Try: /bpu help");
				return true;
			}
			
			if (args.length >= 1) {
				if (args[0].equalsIgnoreCase("help")) {
					if (!sender.hasPermission("bananaperms.user.help")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					
					sender.sendMessage(ChatColor.AQUA.toString() + ChatColor.BOLD.toString() + ChatColor.UNDERLINE.toString() + "User Commands: ");
					sender.sendMessage("/bpu <User> " + ChatColor.ITALIC + "List permissions of the player.");
					sender.sendMessage("/bpu <User> c " + ChatColor.ITALIC + "Clear permissions of the player.");
					sender.sendMessage("/bpu <User> g {Group} " + ChatColor.ITALIC + "Set/Get group to the player.");
					sender.sendMessage("/bpu <User> + <Permission> " + ChatColor.ITALIC + "Add permission to the player.");
					sender.sendMessage("/bpu <User> - <Permission> " + ChatColor.ITALIC + "Remove permission from the player.");
					sender.sendMessage("/bpu <User> f {Suffix#Prefix} " + ChatColor.ITALIC + "Clear/Set chat format, #=Username.");
					return true;
				}
				
				if (!(isValidPlayer(plugin.getServer().getOfflinePlayer(args[0])))) {
					sender.sendMessage(ChatColor.RED + args[0] + " isn't a player.");
					return true;
				}
			}
			OfflinePlayer p = plugin.getServer().getOfflinePlayer(args[0]);
			uBananaPerms bp = new uBananaPerms(plugin, p.getUniqueId());
			
			// Args == 1
			if (args.length == 1) {
				if (!sender.hasPermission("bananaperms.user.list")) {
					sender.sendMessage(ChatColor.RED + "You don't have permissions.");
					return true;
				}
				
				sender.sendMessage(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD.toString() + "The permissions of " + args[0] + " : ");
				sender.sendMessage("");
				for (String s : bp.getPermissions()) {
					sender.sendMessage("  - " + s);
				}
			}
			
			// Args == 1
			// Args == 2
			
			else if (args.length == 2) {				
				if (args[1].equalsIgnoreCase("c")) {
					if (!sender.hasPermission("bananaperms.user.clear")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					@SuppressWarnings("unchecked")
					ArrayList<String> prms = (ArrayList<String>) bp.getConfigPermissions().clone();
					for (String s : prms) {
						bp.delPermission(s);
					}
					
					sender.sendMessage(ChatColor.RED + "All the permissions removed.");
				}
				else if (args[1].equalsIgnoreCase("g")) {
					if (!sender.hasPermission("bananaperms.user.group")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					sender.sendMessage(ChatColor.AQUA + args[0] + "'s group: " + ChatColor.GREEN + bp.getPlayerGroup());
				}
				else if (args[1].equalsIgnoreCase("f")) {
					if (!sender.hasPermission("bananaperms.user.chat")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					bp.setFormat(null);
					sender.sendMessage(ChatColor.RED + "The format was removed.");
				}
				else {
					sender.sendMessage(ChatColor.RED + "The command wasn't found. Try: /bpu help");
				}
			}
			
			// Args == 2
			// Args == 3
			
			else if (args.length >= 3) {
				if (args[1].equalsIgnoreCase("f")) {
					if (!sender.hasPermission("bananaperms.user.chat")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
				
					String message = "";
					for (String s : args) {
						if (s.equals(args[0]) || s.equals(args[1])) {continue;}
						message = message + (s.equals(args[args.length-1]) ? s : s + " ");
					}
					if (!message.contains("#")) {
						sender.sendMessage(ChatColor.RED + "The format isn't contains #");
						return true;
					}
					message = message.replaceAll("&", "§");
					bp.setFormat(message);
					sender.sendMessage(ChatColor.AQUA + "The format is now : " + ChatColor.LIGHT_PURPLE  + message);
					return true;
				}
				
				if (args.length != 3) {
					sender.sendMessage(ChatColor.RED + "The command wasn't found. try: /bpu help");
					return true;
				}
				
				if (args[1].equalsIgnoreCase("+")) {
					if (!sender.hasPermission("bananaperms.user.add")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					
					if (bp.getConfigPermissions().contains(args[2])) {
						sender.sendMessage(ChatColor.RED + "He has already have the permission (in the config).");
						return true;
					}
					
					bp.addPermission(args[2]);
					sender.sendMessage(ChatColor.AQUA + "The permission has been added!");
				}
				else if (args[1].equalsIgnoreCase("-")) {
					if (!sender.hasPermission("bananaperms.user.del")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					
					if (!(bp.getConfigPermissions().contains(args[2]))) {
						sender.sendMessage(ChatColor.RED + "He doesn't have the permission. (in the config)");
						return true;
					}
					
					bp.delPermission(args[2]);
					sender.sendMessage(ChatColor.AQUA + "The permission has been removed!");
				}
				else if (args[1].equalsIgnoreCase("g")) {
					if (!sender.hasPermission("bananaperms.user.group")) {
						sender.sendMessage(ChatColor.RED + "You don't have permissions.");
						return true;
					}
					
					bp.setPlayerGroup(args[2]);
					sender.sendMessage(ChatColor.GREEN + "The player was added to group: " + ChatColor.AQUA + args[2]);
				}
				else {
					sender.sendMessage(ChatColor.RED + "The command wasn't found. Try: /bpu help");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "The command wasn't found. Try: /bpu help");
			}
			
		}
		
		return false;
	}
	
	private boolean isValidPlayer(OfflinePlayer op) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.getUniqueId() == op.getUniqueId()) {
				return true;
			}
		}
		return (op.hasPlayedBefore());
	}

}
