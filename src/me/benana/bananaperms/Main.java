package me.benana.bananaperms;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.bananaapiperms.gBananaPerms;
import me.benana.bananaapiperms.uBananaPerms;
import me.benana.bananaperms.listeners.ChatPlayerListener;
import me.benana.bananaperms.listeners.PlayerPermissionListener;
import me.benana.bananaperms.listeners.SuperPermissionListener;

public class Main extends JavaPlugin {
	public static Helper helper;	
	
	@Override
	public void onEnable() {
		helper = new Helper(this);
		
		registerCommands();
		getServer().getPluginManager().registerEvents(new PlayerPermissionListener(this), this);
		getServer().getPluginManager().registerEvents(new ChatPlayerListener(this), this);
		getServer().getPluginManager().registerEvents(new SuperPermissionListener(), this);
		
		getConfig().options().copyDefaults(true);
		saveConfig();
		reloadConfig();
		
		uBananaPerms bp = new uBananaPerms(this, "NOTHING");
		gBananaPerms bpg = new gBananaPerms(this, "NOTHING");
		ArrayList<String> permissions;
		for (Player p : Bukkit.getOnlinePlayers()) {
			bp.setUUID(p.getUniqueId());
			bpg.setGroup(bp.getPlayerGroup());
			permissions = bp.getConfigPermissions();
			for (String s : permissions) {
				bp.setPermission(s, true);
			}
			permissions = bpg.getPermissions();
			for (String s : permissions) {
				bp.setPermission(s, true);
			}
		}
	}
	
	private void registerCommands() {
		registerCommand("bpu", new onUserCommands(this));
		registerCommand("bpg", new onGroupCommands(this));
		registerCommand("bu", new onUserCommands(this));
		registerCommand("bg", new onGroupCommands(this));
		registerCommand("bp", new onMainCommands(this));
		
	}
	
	private void registerCommand(String s, CommandExecutor cx) {
		getCommand(s).setExecutor(cx);
	}
}
